<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	 public function __construct() {
        parent::__construct();
        $this->load->model('users_model');
    }

	public function index(){

		$users = $this->users_model->getUsers();
		
		$data['users'] = $users;

		$this->load->view('layout/header');
		$this->load->view('inicio', $data);
		$this->load->view('layout/footer');
	}

	public function ajaxGetUserData($user){
		$data = $this->users_model->getUserData($user);
		$profiles = $this->users_model->getProfiles();

		$profiles_to_add = array();
		$profiles_to_quit = array();

		$perfil = '';
		for($i=0; $i<sizeof($profiles) ; $i++) {
			foreach ( $data as $user){			
				if($user->perfil != $profiles[$i]->perfil  && $perfil != $profiles[$i]->perfil){
					array_push($profiles_to_add, $profiles[$i]);
				}else{
					if($user->perfil != 'General' && $perfil != $profiles[$i]->perfil ){
						array_push($profiles_to_quit, $profiles[$i]);	
					}
					
				}
				$perfil = $profiles[$i]->perfil ;	
			}
			
		}

		$user_data['user_data'] = $data;
		$user_data['to_add'] = $profiles_to_add;
		$user_data['to_quit'] = $profiles_to_quit;

		echo json_encode($user_data);
	}


	public function ajaxGetProfileroles($type){
		$roles  = $this->users_model->getProfileRoles($type);

		echo json_encode($roles);
	}


	public function ajaxSetUserProfileRole(){

		$params = array(
			'User' => $this->input->post('User'),
			'password'=> $this->input->post('password'),
			'name' => $this->input->post('name'),
			'tipo' => $this->input->post('tipo'),
			'activo' => 1,
			'email' => $this->input->post('email'),
			'rol' => $this->input->post('rol'),
			'funcion'=> $this->input->post('funcion'), 
			'compania' => 'Interteam',
			'perfil' => $this->input->post('perfil'),
		);

		$insert_new = $this->users_model->setNewUserProfileRole($params);

		if ($insert_new) {
			$data = array("response" => true, "msg" => 'Se ha creado el usuario con perfil '. utf8_encode( $this->input->post('perfil') ) .' y rol '.$this->input->post('rol').' - '.$this->input->post('funcion').'.');
		} else {
			$data = array("response" => false, "msg" => 'Hubo un error al registrar este usuario.<br />Por favor intentalo más tarde.');
		}
        
        echo json_encode($data);
	}

	public function ajaxquitprofilerole(){
		$quit_user_profile = $this->users_model->quitProfileRole($this->input->post('id'));

		if ($quit_user_profile) {
			$data = array("response" => true, "msg" => 'Se ha quitado el usuario');
		} else {
			$data = array("response" => false, "msg" => 'Hubo un error al retirar este usuario.<br />Por favor intentalo más tarde.');
		}
        
        echo json_encode($data);
	}
	
}
