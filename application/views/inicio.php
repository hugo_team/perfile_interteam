<div class="row">
    <form class="col-xl-10 col-sm-10 mb-3">
        <div class="input-group">
            <div class="input-group-prepend">
            <span class="input-group-text">Usuario</span>
            </div>
            <select  class="form-control" id="user" data-placeholder="Selecciona un usuario" data-allow-clear="true"  >
                <option value=""></option>
                <?php foreach ($users as $user):?>
                <option value='<?php echo json_encode($user); ?>'><?php echo $user->User . ' - '. $user->email ; ?></option>
                <?php endforeach ;?>
            </select>
        </div>
    </form>
</div>

<div class="row">    
    <div class="col-xl-3 col-sm-6 mb-3" id="addCard" style="display:none;" >
        <div class="card text-white bg-success o-hidden profile_card">
            <div class="card-body" data-toggle="collapse" data-target="#profileListAdd" >
                <div class="card-body-icon">
                    <i class="fas fa-plus-circle"></i>
                </div>
                <div class="mr-5 float-left">Agregar Perfil</div>                
                <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                </span>               
            </div> 
            <div  id="profileListAdd" class="card-footer text-white clearfix small z-1 collapse"></div>
            
        </div>
    </div>


    <!--div class="col-xl-3 col-sm-6 mb-3" id="quitCard" style="display:none;">
        <div class="card text-white bg-danger o-hidden profile_card">
            <div class="card-body" data-toggle="collapse" data-target="#profileListQuit">
                <div class="card-body-icon">
                    <i class="fas fa-minus-circle"></i>
                </div>
                <div class="mr-5 float-left">Quitar Perfil</div>
                <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                </span>                                    
            </div>
            <div  id="profileListQuit" class="card-footer text-white clearfix small z-1 collapse" ></div>           
        </div>
    </div-->

    <div class="col-xl-3 col-sm-6 mb-3" id="addRoleCard"  style="display:none;" >
        <div class="card text-white bg-primary o-hidden profile_card">
            <div class="card-body"  >
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5 float-left">Roles <span id="profileType"></span></div>                                                
            </div> 
            <div  id="profileRoleList" class="card-footer text-white clearfix small z-1"></div>
            
        </div>
    </div>

</div>

 
<div  id="roles_list" class="card mb-3" style="display:none;">
          <div class="card-header">
            <i class="fas fa-address-card"></i>
            Resultados para <span id="userNameSelected"></span></div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th style="width: 40px;">Tipo</th>
                    <th>email</th>
                    <th style="width: 40px;">Rol</th>
                    <th>Función</th>
                    <th>Perfil</th>
                    <th></th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Nombre</th>
                    <th style="width: 40px;">Tipo</th>
                    <th>email</th>
                    <th style="width: 40px;">Rol</th>
                    <th>Función</th>
                    <th>Perfil</th>
                    <th></th>
                  </tr>
                </tfoot>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted"></div>
        </div>
