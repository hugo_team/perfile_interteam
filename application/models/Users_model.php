<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_model extends CI_Model {

    public function __construct() {
        parent::__construct();       
        $this->load->database();
    }


    function getUsers(){
        $this->db->select('User, password, name, email');
        $this->db->from('usuarios_int');
        $this->db->where('activo', 1); 
        $this->db->group_by('User');
           
        $result= $this->db->get();                
        return $result->result();
    }

     function getUserData($user){
        $this->db->select('*');
        $this->db->from('usuarios_int');

        $where = array('activo' => 1, 'User' => $user);
        $this->db->where( $where);         
           
        $result= $this->db->get();                
        return $result->result();
    }

    function getProfiles(){
        $this->db->select('*');
        $this->db->from('perfiles');           
        $result= $this->db->get();                
        return $result->result();
    }

    function getProfileRoles($type){
        $this->db->select('*');
        $this->db->from('roles');    
        $where = array('tipo' => $type);
        $this->db->where( $where);        
        $result= $this->db->get();                
        return $result->result();
    }


    function setNewUserProfileRole($params){

        $this->db->set($params);
        $this->db->insert('usuarios_int');
        return true;
    }

    function quitProfileRole($id){
        $data = array('activo' => 0);
        $this->db->where('id', $id);
        if ($this->db->update('usuarios_int', $data)) {
            return true;
        } else {
            return false;
        }
    }

}