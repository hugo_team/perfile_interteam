var origen = window.location.href;

$(document).ready(function(){
    $('#user').select2();

    $('#user').change(function(){        
        var user_data = $(this).val();
        getAllUserData(user_data);  
    });

    $('#user').on("select2:unselect", function (e) {
        $('#dataTable tbody').html('');
        $('#userNameSelected').text('');  
        $('#addCard').fadeOut();
        $('#roles_list').fadeOut();  
       
    });

});



function getAllUserData(user_data){
    $('#userNameSelected').text('');  
    if (user_data != '') {
        var user_selected = JSON.parse(user_data).User;
        $.ajax({
            type: "post",
            url: origen + 'inicio/ajaxGetUserData/' + user_selected,
            dataType: "json",
            success: function (data) {

                var profile_to_add = '';
                if (data.to_add.length > 0) {

                    profile_to_add += '<ul>';
                    data.to_add.forEach(function (add) {
                        profile_to_add += '<li><a href="javascript:void(0);" onclick="getProfileRoles(\'' + add.perfil + '\' )">' + add.perfil + '</a></li>';
                    });
                    profile_to_add += '</ul>';
                    $('#profileListAdd').html(profile_to_add);

                    $('#addCard').fadeIn();
                }

                $('#userNameSelected').text(user_selected);

                if ($.fn.DataTable.isDataTable('#dataTable')) {
                    $('#dataTable').DataTable().destroy();
                }


                $('#dataTable tbody').html(buildTable(data.user_data));

                makeDataTable('dataTable');
                $('#roles_list').fadeIn();
            },
            error: function (data) {
                console.log(data);
            }
        });
    } 
}

function getProfileRoles(tipo){
    var rolesArray = [];
        rolesArray['A'] = 'Administrador';
        rolesArray['E'] = 'Ejecutivo';
        rolesArray['SA'] = 'SUPERADMIN';   
        rolesArray['V'] = 'Ventas';
        rolesArray['CS'] = 'Customer Service';

    $.ajax({
        type: "post",
        url: origen + 'inicio/ajaxGetProfileroles/' +  tipo.charAt(0) ,
        dataType: "json",
        success: function (data) {
            console.log(data);

            role_to_add ='';

            role_to_add += '<ul>';
            data.forEach(function (role) {
                role_to_add += '<li><a href="javascript:void(0);" onclick="insertProfileRoles(\'' + tipo + '\',\'' + role.rol + '\',\'' + rolesArray[role.rol] +'\')"><strong>' + role.rol + '</strong>   ' + rolesArray[role.rol]+  '</a></li>';
            });
            role_to_add += '</ul>';
            $('#profileType').text(tipo)
            $('#profileRoleList').html(role_to_add);
            $('#addRoleCard').fadeIn();

            
        },
        error: function(data){
            console.log(data);
        }
    });    

}

function insertProfileRoles(perfil, rol, funcion){
    var createUser = confirm("¿Está seguro que desea crear este perfil?");
    if (createUser) {
        var user_selected = JSON.parse($('#user').val());
        var user_data = {
            User: user_selected.User,
            password: user_selected.password,
            name: user_selected.name,
            tipo: perfil.charAt(0) ,
            email: user_selected.email,
            rol: rol ,
            funcion: funcion,
            perfil: perfil
        }
        
        $.ajax({
            type: "post",
            url: origen + 'inicio/ajaxSetUserProfileRole',
            data: user_data,
            dataType: "json",
            success: function (data) {
                alert(data.msg);
                if(data.response == true){                
                    getAllUserData( $('#user').val() ); 
                }
            },
            error: function (data) {
                console.log(data);
            }
        });

    }

}


function quitProfileRole(id) {
    var deleteUser = confirm("¿Está seguro que desea eliminar este perfil?");
    if (deleteUser) {
        var data = {
            id: id,
        };
        console.log(id);
        $.ajax({
            type: 'POST',
            url: origen + 'inicio/ajaxquitprofilerole',
            dataType: 'json',
            data: data,
            success: function (data) {
                alert(data.msg);
                if (data.response == true) {
                    getAllUserData($('#user').val());
                }
               
            },
            error: function (data) {
                console.log(data);
             }
        });
    }
    return true;
}


function buildTable(user_data){
    var tbody = '';
    user_data.forEach(function (user) {
        $.each(user, function (i, value) {
            user[i] = value == null ? ' - ' : value == '' ? ' - ' : value;
        });
        var remove = user.perfil == 'Comercial' || user.perfil == 'Importación' || user.perfil == 'Exportación' ? '<a href="javascript:void(0)" onclick="quitProfileRole(' + user.id + ')"><i class="fas fa-trash-alt"></i></a>' : '';

        tbody += '<tr>';
        tbody += '<td>' + user.name + '</td>';
        tbody += '<td style="width: 40px;">' + user.tipo + '</td>';
        tbody += '<td>' + user.email + '</td>';
        tbody += '<td style="width: 40px;" >' + user.rol + '</td>';
        tbody += '<td>' + user.funcion + '</td>';
        tbody += '<td>' + user.perfil + '</td>';
        tbody += '<td>' + remove + '</td>'
        tbody += '</tr>';
    });

    return tbody;
}

function makeDataTable(id_table) {
    $('#' + id_table + ' tfoot').html($('#' + id_table + ' thead').html());
    $('#' + id_table + ' tfoot th').each(function () {
        var title = $(this).text();
        if (title === "") {
            $(this).html('');
        } else {
            $(this).html('<input type="text" class="input_table" placeholder="' + title + '" />');
        }
    });

    $('#' + id_table).DataTable({
        "language": {
            "url": origen + "assets/vendor/datatables/espanol.json"
        },
        //"scrollX": true,
        initComplete: function () {
            var api = this.api();
            api.columns().every(function () {
                var that = this;
                $('input.input_table', this.footer()).on('keyup change', function (e) {
                    if (that.search() !== this.value) {
                        that.search(this.value).draw();
                    }
                });
            });
        }
    });
}
